const createHafas = require('db-hafas')
const hafas = createHafas('my-awesome-program')

// Stations: Tuebingen Hbf: 8000141
//           Stuttgart Hbf: 8000096
//           Stuttgart Stadtmitte: 8006700
var departureStation = '8006700';
var arrivalStation = '8000096';
var nrresults = 10;
var checkInterval = 3;
var now = true;

function sleep(s) {
  return new Promise(resolve => setTimeout(resolve, 1000*s));
}

async function getConnectionsToday(m_departureStation, m_arrivalStation)
{
  const response = await hafas.journeys(departureStation, arrivalStation, {results: nrresults, transfers: 0, departure: now ? Date.now() : new Date("2020-01-25T20:50:00+01:00")});
  return response.journeys;
}

async function trackTrain1(tripId, lineName, plannedDeparture)
{
  console.log("Tracking trip '" + tripId + "', line '" + lineName + "' departing at " + plannedDeparture + ".");

  // Wait until one minute before planned departure
  secondsUntillDeparture = Math.trunc((plannedDeparture.getTime() - Date.now()) / 1000) - 60;
  console.log("Seconds untill departure: " + secondsUntillDeparture);
  await sleep(secondsUntillDeparture);

  console.log("At " + Date.now() + " train is one Minute ahead: " + plannedDeparture)

  for(var i = 0; i < 20; i++)
  {
    var trip = await hafas.trip(tripId, lineName, {stopovers: true});
    let now = new Date(Date.now());
    console.log(trip)
    //console.log("Time: " + now.toTimeString() + ", departure: " + new Date(trip.departure).toTimeString() + ", delay: " + trip.departureDelay)
    await sleep(3)
  }

  return;

  var tripBefore;

  var departed = false;
  while(!departed)
  {
    var trip = await hafas.trip(tripId, lineName, {stopovers: false});
    if(new Date(trip.departure) < Date.now())
      departed = true;
    console.log("Departure of " + lineName + ": " + trip.arrivalDelay);

    await sleep(checkInterval);
  }

  var arrived = false;
  while(!arrived)
  {
    var trip = await hafas.trip(tripId, lineName, {stopovers: false});
    if(new Date(trip.arrival) < Date.now())
      departed = true;
  }
}

async function trackTrain(refreshToken, plannedDeparture)
{
  var j = await hafas.refreshJourney(refreshToken, {stopovers: false});
  //console.log("Tracking line '" + j.legs[0].line.name + "' departing at " + plannedDeparture + ", departure " + j.legs[0].departureDelay/60 + ".");

  for(var i = 0; i < 20; i++)
  {
    j = await hafas.refreshJourney(refreshToken, {stopovers: false});
    let now = new Date(Date.now());
    console.log(j.legs[0].line.name + ", Time: " + now.toTimeString() + ", departure: " + new Date(j.legs[0].departure).toTimeString() + ", delay: " + j.legs[0].departureDelay/60)
    await sleep(20)
  }

}

async function main()
{
  var journeysToday = await getConnectionsToday(departureStation, arrivalStation);
  for(var j of journeysToday) {
    let plannedDeparture = new Date(j.legs[0].plannedDeparture)
    trackTrain(j.refreshToken, plannedDeparture);
  }
}

main();

/*hafas.journeys(departureStation, arrivalStation, {results: nrresults, transfers: 0,
  departure: new Date("2020-01-25T06:50:00+01:00")}).then(({journeys}) =>
{
  for(var j of journeys) {
    console.log(j.legs[0].plannedDeparture);
  }
})*/
